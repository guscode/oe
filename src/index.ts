let menuEl = document.querySelector('#menu');

menuEl.addEventListener('click', (event) => {
    let wrapperEl = document.querySelector('.wrapper.dashboard');

    if (wrapperEl.className.indexOf('side-menu-collapse') > -1) {
        wrapperEl.className = 'wrapper dashboard';
    } else {
        wrapperEl.className = 'wrapper dashboard side-menu-collapse';
    }
});